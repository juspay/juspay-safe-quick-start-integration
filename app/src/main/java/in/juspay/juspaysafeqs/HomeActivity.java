package in.juspay.juspaysafeqs;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import in.juspay.godel.analytics.GodelTracker;
import in.juspay.godel.ui.JuspayBrowserFragment;
import in.juspay.godel.ui.OnScreenDisplay;
import in.juspay.juspaysafe.BrowserCallback;
import in.juspay.juspaysafe.BrowserParams;
import in.juspay.juspaysafe.JuspaySafeBrowser;

public class HomeActivity extends AppCompatActivity {
    String PAYMENT_URL;
    String PAYMENT_POST_DATA;
    final String transactionId = "JUS_" + (int) (Math.random() * 10000);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        JuspayBrowserFragment.openJuspayConnection(getApplicationContext());
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        PAYMENT_URL = getString(R.string.payment_url);
        PAYMENT_POST_DATA = getString(R.string.payment_post_data);

        final OnScreenDisplay osdDetails = OnScreenDisplay.Generic("Travel Amount")
                .appendTripleLine("Vimal Kumar", "Koromangala WhiteField", "Travel Time 140 minutes")
                .appendDottedDivider()
                .appendDoubleLine("Cab Number", "KA01AG1337")
                .appendDoubleLine("Cab", "Toyota Etios")
                .setAmount("540")
                .build();

        final Button pay = findViewById(R.id.pay_button);
        if(pay!=null){
            pay.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    // Setting properties
                    String[] endUrlRegexes = {".*icicibank.*", ".*citibank.*"};
                    JuspaySafeBrowser.setEndUrls(endUrlRegexes);

                    // client identification
                    BrowserParams browserParams = new BrowserParams();
                    browserParams.setClientId("juspay_demo_qs_android");
                    browserParams.setMerchantId("juspay_demo_qs");
                    browserParams.setTransactionId("123456");

                    // customer identification
                    browserParams.setCustomerId("654321");
                    browserParams.setCustomerEmail("parth.vora@juspay.in");
                    browserParams.setCustomerPhoneNumber("8123715658");

                    // order meta information
                    browserParams.setRemarks("Payment of Rs.10 to Vodafone");
                    browserParams.setOnScreenDisplay(osdDetails);

                    // authentication data
                    browserParams.setUrl(PAYMENT_URL);
                    browserParams.setPostData(PAYMENT_POST_DATA);

                    // Calling JuspaySafeBrowser.start
                    JuspaySafeBrowser.start(HomeActivity.this, browserParams, callBack);
                }
            });
        }
    }


    public BrowserCallback callBack = new BrowserCallback() {
        @Override
        public void endUrlReached(WebView webView, JSONObject sessionInfo) {
            // Required action performed
            try {
                if (sessionInfo.getString("url").contains("icicibank")) {
                    Toast.makeText(getApplicationContext(), "Transaction successful!", Toast.LENGTH_LONG).show();

                    // Sending the payment status to the Safe Browser
                    GodelTracker.getInstance().trackPaymentStatus(transactionId, GodelTracker.SUCCESS);
                } else {
                    Toast.makeText(getApplicationContext(), "Transaction failure!", Toast.LENGTH_LONG).show();

                    // Sending the payment status to the Safe Browser
                    GodelTracker.getInstance().trackPaymentStatus(transactionId, GodelTracker.FAILURE);
                }
                JuspaySafeBrowser.exit();
            } catch (JSONException e1) {
                e1.printStackTrace();
            }
        }

        @Override
        public void onTransactionAborted(JSONObject sessionInfo) {
            // Sending the payment status to the Safe Browser

            GodelTracker.getInstance().trackPaymentStatus(transactionId, GodelTracker.CANCELLED);
            JuspaySafeBrowser.exit();

            Toast.makeText(HomeActivity.this,  "Transaction cancelled by User!", Toast.LENGTH_LONG).show();
        }
    };

}
