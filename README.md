# Juspay Safe QS Integration Demo  #

This project will hep understanding how integration works with Juspay Safe. Code snippets will let you integrate no more than half a day.

### What is this repository for? ###

* Code Snippets
* Demo
* Detail documentation please find at https://juspay.in/docs/qs/godel/

### How do I get set up? ###

* This project uses gradle. If gradle is not installed, it should auto-download it when project is opened in Android Studio
* Feel free to fork it and modify